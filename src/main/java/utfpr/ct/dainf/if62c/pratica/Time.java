/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.HashMap;

/**
 *
 * @author LucianoTadeu
 */
public class Time {
    private HashMap<String, Jogador> jogadores = new HashMap<>();
    
    public HashMap<String, Jogador> getJogadores() {
        return jogadores;
    }
    
    /* Na classe Time implemente o método público addJogador(String, Jogador) 
       que deverá incluir o jogador recebido como argumento no HashMap usando 
       como chave a posição informada no primeiro argumento.
    */
    public void addJogador(String pos, Jogador jog) {
        jogadores.put(pos, new Jogador(jog.getNumero(), jog.getNome()));
    }
}
