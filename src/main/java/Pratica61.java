
import java.util.HashMap;
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        //System.out.println("Olá, Java!");
        
        // No programa principal, usando a classe Time, crie 2 times com o 
        // mesmo número de jogadores e com os mesmos nomes de posições. 
        // As posições não precisam estar necessariamente na mesma ordem 
        // em ambos os times.
        
        Time t1 = new Time();
        Time t2 = new Time();
        Jogador j = new Jogador(0, "");
        
        j.setNumero(1); j.setNome("Jailson");
        t1.addJogador("Goleiro", j);
        j.setNumero(1); j.setNome("Prass");
        t2.addJogador("Goleiro", j);        
        
        j.setNumero(3); j.setNome("Vitor");
        t1.addJogador("Volante", j);
        j.setNumero(3); j.setNome("Edu");
        t2.addJogador("Volante", j);        

        j.setNumero(5); j.setNome("Moises");
        t1.addJogador("Meia", j);
        j.setNumero(5); j.setNome("Cleiton");
        t2.addJogador("Meia", j);        

        j.setNumero(11); j.setNome("Gabriel");
        t1.addJogador("Atacante", j);
        j.setNumero(11); j.setNome("Dudu");
        t2.addJogador("Atacante", j);        

        j.setNumero(9); j.setNome("Barrios");
        t1.addJogador("Centroavante", j);
        j.setNumero(9); j.setNome("Alecsandro");
        t2.addJogador("Centroavante", j);        
        
        // Percorra todos os jogadores de um dos times e encontre o jogador 
        // da mesma posição do outro time, gerando um relatório no seguinte 
        // formato:
        // Posição    Time 1        Time 2
        // Goleiro    1 - Fulano    1 - João
        // Lateral    4 - Ciclano   7 - José
        // Atacante   10 - Beltrano 15 - Mário
        // Sugestão: Use o método keySet para obter o conjunto das chaves 
        // para iterar sobre o HashMap.
        // keySet() - Retorna um conjunto (Set<K>) contendo as chaves do mapa.
        
        HashMap<String, Jogador> jogs1, jogs2;
        jogs1 = t1.getJogadores();
        jogs2 = t2.getJogadores();
        
        //keySet()
        
        System.out.println("Posição" + "\t\t" + "Time 1" + "\t\t" + "Time 2");
        Set<String> entries = jogs1.keySet();
        for (String entry: entries) {
            System.out.println(entry + "\t\t" + jogs1.get(entry).toString() + "\t\t" + jogs2.get(entry).toString() );
        }
    }
}
